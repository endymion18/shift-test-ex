import datetime
from typing import Optional

from pydantic import BaseModel


class CreateEmployeeInfo(BaseModel):
    first_name: str
    last_name: str
    salary: float
    salary_increase: datetime.date


class UpdateEmployeeInfo(BaseModel):
    salary: Optional[float]
    salary_increase: Optional[datetime.date]
