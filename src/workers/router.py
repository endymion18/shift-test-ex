import random

from fastapi import APIRouter, Depends
from fastapi_users import BaseUserManager, models, exceptions, schemas
from sqlalchemy import select, insert, update
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Type

from src.authentication.auth_backend import current_user, current_superuser
from src.authentication.manager import get_user_manager
from src.authentication.models import User
from src.authentication.schemas import UserCreate
from src.database import get_async_session
from src.workers.models import EmployeeInfo
from src.workers.schemas import CreateEmployeeInfo, UpdateEmployeeInfo

router = APIRouter(
    tags=["Employees"],
)


@router.get("/employee_info/me")
async def get_employee_info(session: AsyncSession = Depends(get_async_session), user: User = Depends(current_user)):
    info = select(EmployeeInfo).where(EmployeeInfo.user_id == user.id)
    user_info = await session.execute(info)
    return user_info.scalar()


@router.post("/employee_info", tags=["Admin_panel"])
async def add_employee_info(info: CreateEmployeeInfo, session: AsyncSession = Depends(get_async_session),
                            user: User = Depends(current_superuser),
                            user_manager: BaseUserManager[models.UP, models.ID] = Depends(get_user_manager)):
    login = f"{info.first_name}.{info.last_name}"
    password = await create_password()
    user_create = UserCreate(email=login, password=password)
    try:
        created_user = await user_manager.create(
            user_create, safe=True)
    except exceptions.UserAlreadyExists:
        user_id = await session.execute(select(User))
        user_id = f'{len(user_id.scalars().all())}'
        user_create = UserCreate(email=login+user_id, password=password)
        created_user = await user_manager.create(
            user_create, safe=True)
    stmt = insert(EmployeeInfo).values(**info.dict(), user_id=created_user.id)
    await session.execute(stmt)
    await session.commit()
    return [created_user, {"login": created_user.email, "password": password}]


@router.put("/employee_info", tags=["Admin_panel"])
async def change_employee_info(user_id: int, info: UpdateEmployeeInfo,
                               session: AsyncSession = Depends(get_async_session),
                               user: User = Depends(current_superuser)):
    old_info = await session.execute(select(EmployeeInfo).where(EmployeeInfo.user_id == user_id))
    old_info = old_info.scalar()
    info = await change_info(old_info, info)
    stmt = update(EmployeeInfo).where(EmployeeInfo.user_id == user_id) \
        .values(salary=info.salary, salary_increase=info.salary_increase)
    await session.execute(stmt)
    await session.commit()
    return {"status": "success"}


async def create_password():
    chars = 'abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    password = ''
    for i in range(8):
        password += random.choice(chars)
    return password


async def change_info(old, new):
    for attr, value in new:
        if value:
            setattr(old, attr, value)
    return old
