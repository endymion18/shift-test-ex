import datetime

from sqlalchemy import Integer, String, ForeignKey, Float, Date
from sqlalchemy.orm import Mapped, mapped_column

from src.database import Base


class EmployeeInfo(Base):
    __tablename__ = "employeeInfo"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    first_name: Mapped[str] = mapped_column(String(length=50), nullable=False)
    last_name: Mapped[str] = mapped_column(String(length=50), nullable=False)
    salary: Mapped[float] = mapped_column(Float)
    salary_increase: Mapped[datetime.date] = mapped_column(Date)

    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
