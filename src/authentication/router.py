from fastapi import APIRouter, Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.authentication.auth_backend import current_user, current_superuser
from src.authentication.models import User
from src.database import get_async_session

router = APIRouter(
    tags=["Users"],
)


@router.get("/user/me")
async def get_current_user(session: AsyncSession = Depends(get_async_session), user: User = Depends(current_user)):
    info = select(User).where(User.id == user.id)
    user_info = await session.execute(info)
    return user_info.scalar()

