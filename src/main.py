from asyncio import run

import uvicorn
from fastapi import FastAPI, Depends

from src.authentication.auth_backend import fastapi_users, auth_backend, current_user
from src.authentication.schemas import UserRead, UserCreate
from src.database import create_db_and_tables
from src.workers.models import *
from src.authentication.models import *
from src.workers.router import router as employees_router
from src.authentication.router import router as users_router

app = FastAPI(
    title="SHIFT Workers"
)

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["Authentication"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["Authentication"],
)

app.include_router(employees_router)
app.include_router(users_router)


if __name__ == "__main__":
    run(create_db_and_tables())
    uvicorn.run("src.main:app", port=8000, reload=True)
